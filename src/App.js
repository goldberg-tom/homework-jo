// CSS self
import './App.css';
import './styles/theme/theme-1.css';

import MainLayout from './components/layout/MainLayout/MainLayout';
import MaterialForm from './components/layout/MaterialsForm/MaterialsForm';

function App() {
  return (
    <div className="App">
      <MainLayout>
        <MaterialForm />
      </MainLayout>
    </div>
  );
}

export default App;
