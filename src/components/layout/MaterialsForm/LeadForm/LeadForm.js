import { useState, useEffect } from 'react';

import './LeadForm.css';

const Input = props => {
    return <div className="control">
            <input placeholder={props.input.placeholder} type={props.input.type} value={props.input.input} onChange={e=>props.set(e.target.value)} required></input>
            { props.input.err && <p>Error</p>}
        </div>;
};

const LeadForm = () => {    
    const [fullname, setFullname] = useState({
        placeholder: 'Full name',
        type: 'text',
        input: '',
        err: ''
    });

    useEffect(()=>{
        
    }, [fullname]);

    return <form className="leadform">
        <h3 className="h-md centered">Want to get the full version?</h3>
        <p className="p-md centered">Fill in the form below:</p>
        <Input input={fullname} set={setFullname}/>
    </form>
};

export default LeadForm;