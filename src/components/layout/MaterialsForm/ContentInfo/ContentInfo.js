import './ContentInfo.css';

const ContentInfo = () => {
    return <div className="contentinfo">
        <h1 className="h-lg">2021 Inclusive Workplace Calendar</h1>
        <h5 className="h-md">Whitepaper</h5>    
        <p className="p-md">Joonko’s 2021 Inclusive Workplace Calendar aims to help your HR teams with workplace diversity so everyone feels like part of the plan. Amplify meaningful days across the organizational calendar and embrace communal celebrations. This year’s addition also includes tips on maintaining the sense of community while remote with ideas to support workplace inclusion initiaives. We’ve highlighted important (holy)days, including:</p>
        <ul>
            <li>Major religious and ethical days of celebration and observance</li>
            <li>Social Media tips to showcase your D&I initiatives</li>
            <li>Activities to help you celebrate all the biggest holidays with inclusion in mind</li>
        </ul>
        <p className="p-md">Download our Inclusive Workplace Calendar and celebrate all the best days throughout the year the right way.</p>
    </div>
};

export default ContentInfo;