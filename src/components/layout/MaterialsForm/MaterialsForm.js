// import CSS
import './MaterialForm.css';

import ContentInfo from './ContentInfo/ContentInfo';
import LeadForm from './LeadForm/LeadForm';

const MaterialForm = () => {
    return <div className="materialsform">
        <ContentInfo />
        <LeadForm />
    </div>;
};

export default MaterialForm;